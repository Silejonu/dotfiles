# git prompt
# https://stackoverflow.com/questions/1128496/to-get-a-prompt-which-indicates-git-branch-in-zsh
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:*' enable git
vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}
RPROMPT=$'$(vcs_info_wrapper)'

# Prevent `compistall` from automatically running
zstyle :compinstall filename '${HOME}/.zshrc'

# Force emacs mode
bindkey -e

# Fix delete key entering tilde
bindkey "^[[3~" delete-char

# Enable # as a comment marker in interactive shells
setopt INTERACTIVE_COMMENTS

# Do not add commands starting with a space to the history
setopt HIST_IGNORE_SPACE
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE

# Make sure history is not overwritten by previously closed sessions
setopt APPEND_HISTORY

# Replace old duplicate history entries with newer ones
setopt HIST_IGNORE_ALL_DUPS

# Ignore duplicate commands when searching with ctrl+r
setopt HIST_FIND_NO_DUPS

# Prompt before overwrite
alias cp='cp -iv'
alias mv='mv -iv'

# Apply the default theme for feh
alias feh='feh -T default'

# Colour output
alias ls='ls --color=auto --group-directories-first'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias tree='tree --dirsfirst -C'
export LESS='-R --use-color -Dd+r$Du+b'
export MANPAGER="less -R --use-color -Dd+r -Du+b"

# Colour output in man using a less wrapper
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# 'Command not found' hook that will automatically search the official repositories, when entering an unrecognized command.
source /usr/share/doc/pkgfile/command-not-found.zsh

# Add ~/.local/bin to the PATH
export PATH=$HOME/.local/bin:$PATH

# Autocompletion
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*:commands' rehash true

# Prompt in zsh colours
PS1="%F{168}%n@%m%f %F{244}%~%f %F{208}%%_%f "

# Show dirty and writeback memory
alias dirtymem="watch --interval=0.1 'cat /proc/meminfo | grep -e Dirty -e Writeback'"

# Requires package zsh-autosuggestions
source $(locate zsh-autosuggestions.zsh) || echo 'missing package: zsh-autosuggestions, or `updatedb` needed'
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

# Requires package zsh-syntax-highlighting
source $(locate zsh-syntax-highlighting.zsh) || echo 'missing package: zsh-syntax-highlighting, or `updatedb` needed'
ZSH_HIGHLIGHT_STYLES[comment]='bg=magenta,bold' # workaround for https://github.com/zsh-users/zsh-syntax-highlighting/issues/510
