# dotfiles

my personal .files

## locations

```
~/.bashrc
~/.config/fish/functions/fish_prompt.fish
~/.config/MangoHud/MangoHud.conf
~/.zshrc
/etc/vimrc
/etc/yt-dlp.conf
/etc/xdg/nvim/sysinit.vim
/etc/xdg/nvim/sysinit.lua
/etc/xdg/nvim/after/ftplugin/c.lua
```
