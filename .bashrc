# Do not add commands starting with a space nor duplicates to the history
export HISTCONTROL="erasedups:ignorespace"

# Set session history and history file size
export HISTSIZE=10000
export HISTFILESIZE=$HISTSIZE

# Append history when closing terminal instead of overwriting it
shopt -s histappend

# Colour output
alias ls='ls --color=auto --group-directories-first'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias tree='tree --dirsfirst -C'
export LESS='-R --use-color -Dd+r$Du+b'
export MANPAGER="less -R --use-color -Dd+r -Du+b"

# Prompt before overwrite
alias cp='cp -iv'
alias mv='mv -iv'

# Apply the default theme for feh
alias feh='feh -T default'

# Colour output in man using a less wrapper
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# 'Command not found' hook that will automatically search the official repositories, when entering an unrecognized command.
source /usr/share/doc/pkgfile/command-not-found.bash

# Add ~/.local/bin to the PATH
export PATH=$HOME/.local/bin:$PATH

# Prompt in Bash colours
PS1="\[\033[38;5;168m\]\u@\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;244m\]\W\[$(tput sgr0)\] \\$\[$(tput sgr0)\]\[\033[38;5;71m\]_\[$(tput sgr0)\] \[$(tput sgr0)\]"

# Show dirty and writeback memory
alias dirtymem="watch --interval=0.1 'cat /proc/meminfo | grep -e Dirty -e Writeback'"
