syntax on " Turn syntax highlighting on
set number " Show absolute line numbers on the left
set relativenumber " Show the line number relative to the line with the cursor in front of each line
set linebreak " Break lines at word
set autoindent " Copy indent from current line when starting a new line
set expandtab " Use spaces instead of a tab character on TAB
set smarttab " a <Tab> in front of a line inserts blanks according to 'shiftwidth', 'tabstop' or 'softtabstop' is used in other places
set shiftwidth=2 " Arrow function (>>) creates 2 spaces
set hlsearch " When searching (/), highlights matches as you go
set incsearch " When searching (/), display results as you type (instead of only upon ENTER)
set ignorecase " When searching (/), ignore case entirely
set smartcase " When searching (/), automatically switch to a case-sensitive search if you use any capital letters
set showmatch " Show matching brackets when text indicator is over them
set noerrorbells " Silence the error bell
set colorcolumn=80 " Highlight column 80
