vim.g.loaded_netrw = false
vim.g.loaded_netrwPlugin = false
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.linebreak = true
vim.opt.expandtab = true
vim.opt.shiftwidth = 2
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.list = true
vim.opt.listchars = "trail:%,nbsp:·"
vim.opt.colorcolumn = '80'
vim.opt.completeopt = 'menuone,preview'
vim.opt.mouse = ''
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>')
-- Force syntax highlighting for Podman Quadlet files
vim.filetype.add({ extension = { container = 'dosini', pod = 'dosini', volume = 'dosini', network = 'dosini' } })

-- https://github.com/folke/lazy.nvim
-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

-- lazy.nvim packages
require("lazy").setup({
  -- required for /etc/xdg/nvim/after/ftplugin to be loaded
  performance = {
    reset_packpath = false,
    rtp = {
      reset = false,
    },
  },
  spec = {
                                  -- Themes --
  -- { 'Mofiqul/vscode.nvim', lazy = false, priority = 1000, config = function(_, opts) require('vscode').setup(opts) vim.cmd.colorscheme('vscode') end, },
  -- -- { 'scottmckendry/cyberdream.nvim', lazy = false, priority = 1000, config = function(_, opts) require('cyberdream').setup(opts) vim.cmd.colorscheme('cyberdream') end, },
  -- { 'savq/melange-nvim', lazy = false, priority = 1000, config = function() vim.cmd.colorscheme('melange') end, },
  -- { 'bluz71/vim-nightfly-colors', lazy = false, priority = 1000, config = function() vim.g.nightflyItalics = false vim.cmd.colorscheme('nightfly') end, },
  -- -- { 'nyoom-engineering/oxocarbon.nvim', lazy = false, priority = 1000, config = function() vim.cmd.colorscheme('oxocarbon') end, },
  -- { 'olimorris/onedarkpro.nvim', lazy = false, priority = 1000, config = function(_, opts) require('onedarkpro').setup(opts) vim.cmd.colorscheme('onedark') end, },
  -- { 'ribru17/bamboo.nvim', lazy = false, priority = 1000, opts = { code_style = { comments = { italic = false }, conditionals = { italic = false }, namespaces = { italic = false }, parameters = { italic = false }, }, }, config = function(_, opts) require('bamboo').setup(opts) vim.cmd.colorscheme('bamboo') end, },
  -- { 'sainnhe/gruvbox-material', lazy = false, priority = 1000, config = function() vim.cmd.colorscheme('gruvbox-material') end, },
  -- -- { 'ellisonleao/gruvbox.nvim', lazy = false, priority = 1000, config = function(_, opts) require('gruvbox').setup(opts) vim.cmd.colorscheme('gruvbox') end, },
  -- -- { 'kjssad/quantum.vim', lazy = false, priority = 1000, config = function(_, opts) require('quantum').setup(opts) vim.cmd.colorscheme('quantum') end, },
  -- -- { 'comfysage/evergarden', lazy = false, opts = { transparent_background = true, contrast_dark = 'medium', overrides = { }, }, config = function(_, opts) require('evergarden').setup(opts) vim.cmd.colorscheme('evergarden') end, },
  -- -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'default' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'atlantis' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'andromeda' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'shusia' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'maia' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- { 'sainnhe/sonokai', lazy = false, priority = 1000, config = function() vim.g.sonokai_style = 'espresso' vim.g.sonokai_disable_italic_comment = true vim.cmd.colorscheme('sonokai') end }, 
  -- -- { 'sainnhe/everforest', lazy = false, priority = 1000, config = function() vim.g.everforest_background = 'hard' vim.cmd.colorscheme('everforest') end }, -- everforest_background: soft, medium, hard
  -- -- { 'AlexvZyl/nordic.nvim', lazy = false, priority = 1000, config = function(_, opts) require('nordic').setup(opts) vim.cmd.colorscheme('nordic') end, },
  -- { 'rebelot/kanagawa.nvim', priority = 1000, opts = { commentStyle = { italic = false }, keywordStyle = { italic = false }, Boolean = { bold = false }, }, config = function(_, opts) require('kanagawa').setup(opts) vim.cmd.colorscheme('kanagawa-wave') end },
  -- -- { 'folke/tokyonight.nvim', lazy = false, priority = 1000, opts = { style = 'night' }, config = function(_, opts) require('tokyonight').setup(opts) vim.cmd.colorscheme('tokyonight') end, },
  -- -- { 'EdenEast/nightfox.nvim', lazy = false, priority = 1000, config = function(_, opts) require('nightfox').setup(opts) vim.cmd.colorscheme('nightfox') end, },
  { 'oxfist/night-owl.nvim', lazy = false, priority = 1000, opts = { italics = false }, config = function(_, opts) require('night-owl').setup(opts) vim.cmd.colorscheme('night-owl') end, },
                              -- Other plugins --
  -- { 'nvim-tree/nvim-tree.lua', version = "*", lazy = false, dependencies = { 'nvim-tree/nvim-web-devicons', }, config = function() require("nvim-tree").setup { filters = { dotfiles =  true, },renderer = { icons = { show = { file = false, folder = false, folder_arrow = false, git = false, modified = false, diagnostics = false, bookmarks = false, }, glyphs = { default = '', symlink = '', modified = '', folder = {}, }, }, }, } vim.keymap.set('n', '<leader>e', ":NvimTreeToggle<Enter>") end, },
  { 'nvim-tree/nvim-tree.lua', version = "*", lazy = false, dependencies = { 'nvim-tree/nvim-web-devicons', }, config = function() require("nvim-tree").setup { filters = { dotfiles = true, }, } vim.keymap.set('n', '<leader>e', ":NvimTreeToggle<Enter>") end, },
  {'akinsho/toggleterm.nvim', version = "*", opts = { direction = 'float', start_in_insert = false, }, config = function(_, opts) require('toggleterm').setup(opts) vim.keymap.set('n', '<leader>t', '<Esc>:ToggleTerm<Enter>i') vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>:q<Enter>') end, },
  { 'rktjmp/lush.nvim' },
  { 'iamcco/markdown-preview.nvim', cmd = { 'MarkdownPreviewToggle', 'MarkdownPreview', 'MarkdownPreviewStop' }, ft = { 'markdown' }, build = function() vim.fn['mkdp#util#install']() end, },
  -- { 'ellisonleao/glow.nvim', config = true, cmd = 'Glow' },
  -- { 'denstiny/styledoc.nvim', dependencies = { 'nvim-treesitter/nvim-treesitter', 'vhyrro/luarocks.nvim', '3rd/image.nvim', }, opts = true, ft = 'markdown', },
  -- { 'lifepillar/vim-cheat40', },
  -- { 'vim-scripts/AutoComplPop', },
  },
  -- automatically check for plugin updates
  checker = { enabled = true },
})
